from rest_framework import routers
from .api import ForecastViewSet
from .api import AddForecastAPI, GetForecastAPI, AddCoordinatesAPI, GetCoordinatesAPI, AddCoordinateAPI
from django.urls import path
from django.conf.urls import include


# router = routers.DefaultRouter()
# router.register('get_forecast', ForecastViewSet, 'get_forecast')
#
# urlpatterns = router.urls


urlpatterns = [
    # path('', include(router.urls)),
    path('add_forecast/', AddForecastAPI.as_view(),  name='add_forecast'),
    path('get_forecast/', GetForecastAPI.as_view(),  name='get_forecast'),
    path('add_coordinates/', AddCoordinatesAPI.as_view(),  name='add_coordinates'),
    path('add_coordinate/', AddCoordinateAPI.as_view(),  name='add_coordinate'),
    path('get_coordinates/', GetCoordinatesAPI.as_view(),  name='get_coordinates'),
]
from .models import Forecasts
from rest_framework import viewsets, permissions
from .serializers import ForecastSerializer
from rest_framework.decorators import action
from .models import Forecasts, Coordinates
from rest_framework import viewsets, permissions, generics, status
from .serializers import ForecastSerializer, CoordinatesSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
import json


# # Forecasts Viewset
# class ForecastViewSet(viewsets.ModelViewSet):
#     permission_classes = [
#         permissions.IsAuthenticated
#     ]
#
#     serializer_class = ForecastSerializer
#
#     def get_queryset(self):
#         return self.request.user.events.all()
#
#     def perform_create(self, serializer):
#         serializer.save(owner=self.request.user)
#




# Forecasts Viewset
class ForecastViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]

    serializer_class = ForecastSerializer

    # def get_queryset(self):
    #     return self.request.user.forecast.all()

    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user)


class AddForecastAPI(generics.GenericAPIView):
    serializer_class = ForecastSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        forecast = serializer.save()
        res = Response({
            'Forecasts': ForecastSerializer(forecast, context=self.get_serializer_context()).data
        })

        res = res.__dict__
        return Response(res['data']['Forecasts'])


class GetForecastAPI(generics.GenericAPIView):
    serializer_class = ForecastSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request, *args, **kwargs):
        if 'request' in request.query_params:
            name = json.loads(request.query_params['request'])
        if request.user:
            user = request.user
            try:
                name
            except NameError:
                print("well, it WASN'T defined after all!")
            else:
                name = name['name']
                forecast = Forecasts.objects.filter(name=name, owner=user.id)
                res = Response(
                    {'Forecasts': ForecastSerializer(forecast, context=self.get_serializer_context(), many=True).data})
                res = res.__dict__
                return Response(res['data']['Forecasts'])
            if request.data.get("id"):
                forecast_id = request.data.get("id")
                try:
                    forecast = Forecasts.objects.filter(pk=forecast_id, owner=user.id)
                    res = Response(
                        {'Forecasts': ForecastSerializer(forecast, context=self.get_serializer_context(), many=True).data})
                    res = res.__dict__
                    return Response(res['data']['Forecasts'])
                except Forecasts.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                try:
                    if user.is_superuser:
                        forecasts = Forecasts.objects.filter()
                        res = Response({'Forecasts': ForecastSerializer(forecasts,
                                                                        context=self.get_serializer_context(),
                                                                        many=True).data})

                        res = res.__dict__
                        return Response(res['data']['Forecasts'])
                    forecasts = Forecasts.objects.filter(owner=user.id)
                    res = Response({'Forecasts': ForecastSerializer(forecasts, context=self.get_serializer_context(),
                                                                    many=True).data})
                    res = res.__dict__
                    return Response(res['data']['Forecasts'])
                except Forecasts.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)


class AddCoordinateAPI(generics.GenericAPIView):
    serializer_class = CoordinatesSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        if request.user:
            user = request.user
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            if request.data.get("forecast"):
                forecast_id = request.data.get("forecast")
                try:
                    forecast = Forecasts.objects.get(pk=forecast_id, owner=user.id)
                    if forecast:
                        print(forecast)
                        value = serializer.save()
                        return Response(
                            {'Coordinate': CoordinatesSerializer(value, context=self.get_serializer_context()).data})
                    else:
                        return Response(status=status.HTTP_417_EXPECTATION_FAILED)
                except Forecasts.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)


class AddCoordinatesAPI(generics.GenericAPIView):
    serializer_class = CoordinatesSerializer
    queryset = []

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        print(request)
        if request.user:
            ret_list = []
            user = request.user
            res = json.loads(request.body)
            for obj in res:
                serializer = self.get_serializer(data=obj)
                serializer.is_valid(raise_exception=True)
                if obj.get("forecast"):
                    forecast_id = obj.get("forecast")
                    try:
                        forecast = Forecasts.objects.get(pk=forecast_id, owner=user.id)
                        if forecast:
                            value = serializer.save()
                            ret_list.append(CoordinatesSerializer(value, context=self.get_serializer_context()).data)
                    except Forecasts.DoesNotExist:
                        continue
            return Response({'Coordinates': ret_list})


class GetCoordinatesAPI(generics.GenericAPIView):
    serializer_class = CoordinatesSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        if request.user:
            user = request.user
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            if request.data.get("forecast"):
                forecast_id = request.data.get("forecast")
                try:
                    if user.is_superuser:
                        forecast = Forecasts.objects.get(pk=forecast_id)
                    else:
                        forecast = Forecasts.objects.get(pk=forecast_id, owner=user.id)
                    if forecast:
                        values = Coordinates.objects.filter(forecast_id=forecast_id)
                        res = Response({'Coordinates': CoordinatesSerializer(values,
                                                                              context=self.get_serializer_context(),
                                                                              many=True).data})
                        res = res.__dict__
                        return Response(res['data']['Coordinates'])
                    else:
                        return Response(status=status.HTTP_417_EXPECTATION_FAILED)
                    res = Response({'Coordinates': CoordinatesSerializer(values, context=self.get_serializer_context(),
                                                                          many=True).data})
                    res = res.__dict__
                    return Response(res['data']['Coordinates'])
                except Forecasts.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)

from django.contrib import admin
from .models import Forecasts, Coordinates

# Register your models here.
admin.site.register(Forecasts)
admin.site.register(Coordinates)

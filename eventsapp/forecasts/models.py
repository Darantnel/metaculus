from django.db import models
from accounts.models import Account
from django.utils.deconstruct import deconstructible
from django import forms


class Forecasts(models.Model):
    owner = models.ForeignKey(Account, on_delete=models.CASCADE, default=True)
    created_at = models.DateTimeField(null=True, auto_now=True)
    name = models.CharField(max_length=100, unique=True)


class Coordinates(models.Model):
    forecast = models.ForeignKey(Forecasts, related_name='Forecast', on_delete=models.CASCADE, default=True)
    x = models.FloatField(null=False)
    y = models.FloatField(null=False)


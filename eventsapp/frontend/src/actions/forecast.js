import axios from 'axios';
import { createMessage, returnErrors } from './messages';
import { GET_FORECAST, DELETE_FORECAST, ADD_FORECAST, ADD_COORDINATES, GET_COORDINATES } from './types';
import { tokenConfig} from "./auth";


//GET FORECAST
export const getForecast = (request) => (dispatch, getState) => {
    var headers = tokenConfig(getState);
    return axios
        .get("/api/get_forecast/" ,{headers:headers['headers'], params: {request}})
        .then(res => {
            dispatch({
                type: GET_FORECAST,
                payload: res.data
            });
            return {
                data: res.data
            }
    })
    .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));

};

export const getForecasts = () => (dispatch, getState) => {
    axios
        .get("/api/get_forecast/", tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_FORECAST,
                payload: res.data
            });

    })
    .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));

};

//DELETE FORECAST
export const deleteForecast = (id) => (dispatch, getState) => {
    axios
        .delete(`/api/forecast/${id}/`, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({deleteForecast: 'FORECAST DELETED'}));
            dispatch({
                type: DELETE_FORECAST,
                payload: id
            });
    })
    .catch(err => console.log(err));

};

//ADD FORECAST
export const addForecast = (Forecast) => (dispatch, getState) => {

    return axios
        .post("/api/add_forecast/", Forecast, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({addForecast: 'FORECAST CREATED'}));
            dispatch({
                type: ADD_FORECAST,
                payload: res.data
            });
             return {
                data: res.data
            }

    })
    .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};


//ADD COORDINATES
export const addCoordinates = (Coordinates) => (dispatch, getState) => {
    return axios
        .post("/api/add_coordinate/", Coordinates, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({addCoordinates: 'COORDINATES CREATED'}));
            dispatch({
                type: ADD_COORDINATES,
                payload: res.data
            });
    })
    .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));

};

export const getCoordinate = () => (dispatch, getState) => {
    axios
        .get("/api/get_coordinates/", tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_COORDINATES,
                payload: res.data
            });

    })
    .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));

};


export const getCoordinates = (Coordinates) => (dispatch, getState) => {
    var headers = tokenConfig(getState)
    return axios
        .post("/api/get_coordinates/" , Coordinates, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_COORDINATES,
                payload: res.data
            });
    })
    .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));

};

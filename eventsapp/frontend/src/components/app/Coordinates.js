import  React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getCoordinates } from '../../actions/forecast';
import Forecast from './Forecast';


export class Coordinates extends  Component{

    state ={
        redirect:'',
    };

    static propTypes = {
        coordinates: PropTypes.array.isRequired,
        getCoordinates: PropTypes.func.isRequired,
    };


    onClick = e => this.setState({redirect:true});

    componentDidMount() {
        var dict = {
          forecast: this.props.id,
          x: this.props.x,
          y: this.props.y
        };

        this.props.getCoordinates(dict);
    }

    render() {

        if (this.state.redirect == true){
            return <Forecast />
        }
        return(
                <Fragment>
                    <h2>Coordinate List</h2>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>X</th>
                            <th>Y</th>
                        </tr>
                        </thead>
                        <tbody>
                        { this.props.coordinates.map(coordinate =>(
                            <tr key = {coordinate.id}>
                                <td>{coordinate.x}</td>
                                <td>{coordinate.y}</td>
                            </tr>
                        ))}
                        </tbody>

                    </table>
                    <button value="new forecast" className="btn btn-primary float-right" onClick={this.onClick}>
                          Back to Forecast List
                    </button>
                </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    coordinates: state.coordinates.coordinates
});

export default connect(mapStateToProps, { getCoordinates })(Coordinates);
import  React, { Fragment } from 'react';
import ForecastForm from './ForecastForm';
import Forecast from './Forecast';

export default function Dashboard () {
        return(
                <Fragment>
                    <ForecastForm />
                    <Forecast />
                </Fragment>
        );
}

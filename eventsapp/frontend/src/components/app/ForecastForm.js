import  React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addForecast, getForecast } from '../../actions/forecast';
import { Link, Route, Redirect } from 'react-router-dom';
import CoordinatesForm from './CoordinatesForm';

export class Form extends  Component{
    state ={
        id:'',
        name:'',
        doRedirect:false
    };

    static propTypes = {
        addForecast: PropTypes.func.isRequired,
        getForecast: PropTypes.func.isRequired
    };

    onChange = e => this.setState({[e.target.name]: e.target.value });

    onSubmit = async (e) => {
        e.preventDefault();
        const { name } = this.state;
        const forecast = { name };
        await this.props.addForecast(forecast).then((data) =>{
            this.state.id = data['data']['id'];
            this.state.doRedirect = true;
        });
        this.setState({
            name:name
        });
    };

    render() {
        if (this.state.doRedirect == true){
            return <CoordinatesForm forecast={this.state.id} />
        }

        const { name } = this.state.name;
        return(
                <div className="card card-body mt-4 mb-4">
                    <h2>Add Forecast</h2>
                    <form onSubmit={this.onSubmit}>
                      <div className="form-group">
                        <label>Name</label>
                        <input
                          className="form-control"
                          type="text"
                          name="name"
                          onChange={this.onChange}
                          value={name}
                        />
                      </div>
                      <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                          Submit
                        </button>
                      </div>
                    </form>
                </div>
        );
    }
}

export default connect(null, { addForecast, getForecast })(Form);
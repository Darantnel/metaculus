import  React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addCoordinates } from '../../actions/forecast';
import { Route, Redirect } from 'react-router-dom';
import ForecastForm from './ForecastForm';

export class Form extends  Component{
    state ={
        redirect:'',
        forecast:'',
        x:'',
        y:''
    };

    static propTypes = {
        addCoordinates: PropTypes.func.isRequired
    };

    onChange = e => this.setState({[e.target.name]: e.target.value });

    onClick = e => this.setState({redirect:true});

    onSubmit = e => {
        e.preventDefault();
        const forecast = this.props.forecast;
        const { x,y } = this.state;
        const coordinates = { forecast,x,y };

        this.props.addCoordinates(coordinates);
        this.setState({
            forecast:'',
            x:'',
            y:''
        });
    };

    render() {
        if (this.state.redirect == true){
            return <ForecastForm />
        }
        const { forecast,x,y } = this.state;
        return(
                <div className="card card-body mt-4 mb-4">
                    <h2>Add Coordinates</h2>
                    <form onSubmit={this.onSubmit}>
                    <div className="form-group" hidden>
                        <label>ID</label>
                        <input
                          className="form-control"
                          type="text"
                          name="forecast"
                          onChange={this.onChange}
                          value={forecast}
                        />
                      </div>
                      <div className="form-group">
                        <label>x</label>
                        <input
                          className="form-control"
                          type="number"
                          name="x"
                          onChange={this.onChange}
                          value={x}
                        />
                      </div>
                      <div className="form-group">
                        <label>y</label>
                        <input
                          className="form-control"
                          type="number"
                          name="y"
                          onChange={this.onChange}
                          value={y}
                        />
                      </div>
                      <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                          Submit
                        </button>
                        <button value="new forecast" className="btn btn-primary float-right" onClick={this.onClick}>
                          Add New Forecast
                        </button>
                      </div>
                    </form>
                </div>
        );
    }
}

export default connect(null, { addCoordinates })(Form);
import  React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getForecast, getForecasts, deleteForecast } from '../../actions/forecast';
import Coordinates from './Coordinates';


export class Forecast extends  Component{

    static propTypes = {
        forecast: PropTypes.array.isRequired,
        getForecast: PropTypes.func.isRequired,
        getForecasts: PropTypes.func.isRequired,
        deleteForecast:PropTypes.func.isRequired
    };

    state ={
        redirect:'',
        id:''
    };

    onClick = (e) => {
        this.setState({
            redirect:true,
            id:e
        });
    }


    componentDidMount() {
        this.props.getForecasts();
    }

    render() {
        if (this.state.redirect == true){
            return <Coordinates id={this.state.id} x={"1"} y={"1"} />
        }
        return(
                <Fragment>
                    <h2>Forecast List</h2>
                    <table className="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        { this.props.forecast.map(forecast =>(
                            <tr key = {forecast.id} onClick={this.onClick.bind(this, forecast.id)}>
                                <td>{forecast.name}</td>
                                <td><button onClick={this.props.deleteForecast.bind(this, forecast.id)} className="btn btn-danger btn-sm">Delete</button></td>
                            </tr>
                        ))}
                        </tbody>

                    </table>
                </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    forecast: state.forecast.forecast
});

export default connect(mapStateToProps, { getForecast, getForecasts, deleteForecast })(Forecast);
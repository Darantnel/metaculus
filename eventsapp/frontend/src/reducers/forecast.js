import { GET_FORECAST, DELETE_FORECAST, ADD_FORECAST, ADD_COORDINATES, GET_COORDINATES } from '../actions/types.js';

const initialState = {
    forecast: [],
    coordinates: []
};

export default function (state = initialState, action)  {
    switch  (action.type)    {
        case GET_FORECAST:
            return{
                ...state,
                forecast: action.payload
            };
        case ADD_COORDINATES:
            return{
                ...state,
                coordinates: [...state.coordinates, action.payload]
            };
        case GET_COORDINATES:
            return{
                ...state,
                coordinates: action.payload
            };
        case DELETE_FORECAST:
            return{
                ...state,
                forecast: state.forecast.filter(forecast => forecast.id !== action.payload)
            };
        case ADD_FORECAST:
            return{
                ...state,
                forecast: [...state.forecast, action.payload]
            };
        default:
            return state;
    }
}
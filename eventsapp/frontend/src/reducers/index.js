import { combineReducers } from 'redux';
import forecast from './forecast';
import coordinates from './forecast';
import errors from './errors';
import messages from './messages';
import auth from './auth';



export default combineReducers({
    coordinates,
    forecast,
    errors,
    messages,
    auth
});